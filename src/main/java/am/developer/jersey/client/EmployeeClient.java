package am.developer.jersey.client;

import am.developer.jersey.entitiy.Employee;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class EmployeeClient {
    public static void main(String[] args) {
        Client client = ClientBuilder.newClient( new ClientConfig().register( LoggingFilter.class ) );

        WebTarget webTarget = client.target("http://localhost:8080/JerseyDemos/rest").path("employees");

        Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_XML);

        Employee emp = new Employee();
        emp.setId(1);
        emp.setName("David Feezor");

        Response response = invocationBuilder.post(Entity.entity(emp, MediaType.APPLICATION_XML));
    }
}
